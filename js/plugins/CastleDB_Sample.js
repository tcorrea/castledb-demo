// =============================================================================
// CastleDB Integration Example
// CastleDB_Sample.js
// =============================================================================

var Castle = Castle || {};
Castle.CDB = Castle.CDB || {};
Castle.CDB.version = 1.00;

//=============================================================================
/*:
 * @plugindesc [v1.0] Shows a basic example of how to use CastleDB with RPG Maker MV.
 * @author Kaelan
 *
 * @param Database Name
 * @desc The name of your database file. Don't forget to include the '.cdb' extension.
 * Default: Database.cdb
 * @default Database.cdb
 *
 * @param Database Sheets
 * @desc Which database sheets should be loaded. You should change this to match the contents of your CDB file.
 * @type text[]
 * @default ["Params","Text","Armors","Sounds"]
 *
 * @param MV Overwrites
 * @desc A list of MV JSON files that should be overwritten.
 * For when you're using CDB's database instead of MV's.
 * @type text[]
 * @default ["Armors"]
 *
 * @help
 * ============================================================================
 * Introduction
 * ============================================================================
 *
 * Implements integration with CastleDB. Imports data from a .cdb file and
 * converts it into RPG Maker objects.
 *
 * ============================================================================
 * Changelog
 * ============================================================================
 *
 * Version 1.00:
 * - Initial version finished!
 */
//=============================================================================

//=============================================================================
// Parameter Variables
//=============================================================================

Castle.Param = Castle.Param || {};
Castle.Parameters = PluginManager.parameters('CastleDB_Sample');

Castle.Param.DatabaseName = String(Castle.Parameters['Database Name']);
Castle.Param.Sheets = JSON.parse(Castle.Parameters['Database Sheets']);
Castle.Param.Overwrites = JSON.parse(Castle.Parameters['MV Overwrites']);

//=============================================================================
// Plugin Initialization
//=============================================================================

// Files to load when booting the game
// Let's include the CDB file together with MV's other JSONs
DataManager._databaseFiles.push({name: '$dataCastle', src: Castle.Param.DatabaseName});

// These are the sheets we'll be using from the CDB,
// and what variables we'll be loading them in to.
// They should match the contents of your CDB file.
DataManager._cdbSheets = [];
for (const name of Castle.Param.Sheets) {
    const obj = {dataName: '$data' + name, sheetName: name};
    window[obj.dataName] = window[obj.dataName] || null;
    DataManager._cdbSheets.push(obj);
}

// Files to write to disk once we're done loading the CDB
// We only have to list the JSON files we want to overwrite
DataManager._databaseOverwriteFiles = [];
for (const name of Castle.Param.Overwrites) {
    const obj = {name: '$data' + name, src: name + ".json"};
    DataManager._databaseOverwriteFiles.push(obj);
}

// =============================================================================
// DataManager
//
// Modify the onLoad method to load the CDB file and process its contents,
// so that we can use the data in our game.
// =============================================================================
Castle.CDB.DataManager_onLoad = DataManager.onLoad;
DataManager.onLoad = function(object) {
    if (object === $dataCastle) {
        this.processCdb(object);
    } else {
        Castle.CDB.DataManager_onLoad.call(this, object);
    }
};

DataManager.processCdb = function(cdb) {
    // The CDB is just a big array of sheets (tables)
    // Let's print it to the console so we can see exactly what's stored there
    console.log(cdb);

    // Load a map of sheets that allows referencing them by name
    // We can't iterate blindly through them because each sheet
    // requires processing in a different way
    const sheetsByName = new Map();
    const stringToIndex = new Map();
    for (const sheet of cdb.sheets) {
        sheetsByName.set(sheet.name, sheet);
        // For each sheet, create an intermediary Map relating each item's
        // CDB string IDs to numerical index. We'll be using this to unpack
        // the table references once we finish loading the data.
        if (!sheet.name.includes("@")) { // ignore auto-generated metadata sheets
            const sheetStringToIndexMap = new Map();
            stringToIndex.set(sheet.name, sheetStringToIndexMap);
        }
    }

    // Initialize MV database of objects from CDB sheets
    for (const sheet of DataManager._cdbSheets) {
        this.loadCdbSheet(sheet.dataName, sheetsByName.get(sheet.sheetName), stringToIndex.get(sheet.sheetName));
    }

    // The CDB contains two types of objects that should be modified before we use them in our game:
    // Table References and List Ojbects (Lists, Enums and Bitflags).
    //
    // List objects store values by ID in the CDB (i.e.: Enums store the enum index - 0, 1, 2, etc. -
    // not its value - fighter, cleric, wizard, etc.). We want to translate those to their actual values.
    //
    // Table References are stored by the string ID of the object it's pointing to. We'd rather convert
    // those into the numerical ID of the object, so we can use them as indices into MV's $data objects.
    //
    // Doing this isn't strictly necessary, but it makes it easier to use the data in our game.
    this.unwrapCdb(cdb.sheets, sheetsByName, stringToIndex);

    // Since we're highjacking the normal database object format for various things,
    // we need to add dummy fields to the data generated by CastleDB to make RPG Maker
    // happy (i.e. if we don't do this, the editor crashes).
    // You have to for Classes, Enemies, Weapons, Armor and Items, if you're loading them
    // from the CDB instead of using MV's database.
    for (const sheet of DataManager._cdbSheets) {
        const paddingFunction = this['padCdb' + sheet.sheetName];
        if (typeof paddingFunction === 'function') {
            paddingFunction(window[sheet.dataName]);
        }
    }

    // Write new tables back into JSON. This will overwrite the JSON generated by MV's editor
    // with ones that include our CDB data. Everytime this is done, we have to reload the
    // project in MV, to make sure we see the correct data when we open the MV database.
    // This is only necessary when overwriting the same JSON files MV uses. If you're only
    // using your own custom JSONs, you don't have to do this.
    this.overwriteMvJson();
};

// Load an MV dabatase object from a CastleDB sheet
DataManager.loadCdbSheet = function(dataName, sheet, stringToIndexMap) {
    const objArray = sheet.lines;
    for (const obj of objArray) {
        // Set the string ID => index mapping for each object,
        // increment by 1 to account for the MV null object
        // at array position 0
        obj.index += 1;

        // CastleDB calls an object's numerical ID an 'index'
        // Let's rename it to 'id', to conform to RPG Maker convention
        obj.id = obj.index;
        obj.index = null;

        // To avoid confusion, I call the unique string ID of an object in CastleDB a 'key'.
        // Here, we store the mapping from the object's string identifier (i.e.: 'leather_armor')
        // to their numerical ID in the database (i.e.: 1).
        if (obj.hasOwnProperty('key')) {
            stringToIndexMap.set(obj.key, obj.id);
        }
    }

    // MV requires the first object in each database array to be null
    objArray.unshift(null);
    window[dataName] = objArray;
};


DataManager.unwrapCdb = function(sheets, sheetsByName, stringToIndexMap) {
    for (const sheet of sheets) {
        if (sheet.name.contains("@")) continue; // ignore empty sheets
        // Sheets can have nested lists of references, so we have to do the parsing recursively
        this.unwrapCdbSheet(sheet.name, sheetsByName, sheet.lines, sheet.columns, stringToIndexMap);
    }
};

DataManager.unwrapCdbSheet = function(sheetName, sheetsByName, lines, columns, stringToIndexMap) {
    // Some references don't need to be changed, referring to them by string ID is fine
    const ignoreReferencesTo = ["6:Param", "6:SE"];
    const ignoreEnumsFor = ["wtypeId", "atypeId", "etypeId", "itypeId"];

    // For everything else, let's modify it
    for (const line of lines) {
        if (line === null) continue;
        for (const col of columns) {
            const value = line[col.name];
            if (value === undefined) continue;
            // Ignore references to tables that don't need their references unpacked
            const includeTable = !ignoreReferencesTo.some((element) => col.typeStr.includes(element));

            // Table References are TypeStr = "6:TableName"
            if (col.typeStr.startsWith("6:") && includeTable) {
                const refTableName = col.typeStr.replace("6:", "");
                const sheetIdMap = stringToIndexMap.get(refTableName);
                line[col.name] = sheetIdMap.get(value);

            // Lists are TypeStr = "8"
            } else if (col.typeStr.startsWith("8")) {
                const innerLines = value;
                const innerSheetName = sheetName + "@" + col.name;
                const innerColumns = sheetsByName.get(innerSheetName).columns;
                // Lists can contain any other object, including more lists
                // Unwrap list objects recursively
                this.unwrapCdbSheet(innerSheetName, sheetsByName, innerLines, innerColumns, stringToIndexMap);

            // Enums are TypeStr = "5:Value1,Value2,Value3,..."
            } else if (col.typeStr.startsWith("5:") && col.name) {
                const ignoreEnum = ignoreEnumsFor.some((element) => col.name === element);
                if (ignoreEnum) continue;
                const enumString = col.typeStr.replace("5:", "");
                const enumArray = enumString.split(',');
                line[col.name] = enumArray[value];

            // Bitflags are TypeStr = "10:Flag1,Flag2,Flag3,..."
            } else if (col.typeStr.startsWith("10:")) {
                const bitflagString = col.typeStr.replace("10:", "");
                const bitflagArray = bitflagString.split(',');
                const newValue = [];
                bitflagArray.forEach(function(flag, index) {
                    if (value & (1 << index)) {
                        newValue.push(bitflagArray[index]);
                    }
                });
                line[col.name] = newValue;
            }
        }
    }
};

// Write all the newly generated objects back out to JSON
DataManager.overwriteMvJson = function() {
    console.log("Overwriting MV JSON files");
    const fs = require("fs");
    for (const obj of DataManager._databaseOverwriteFiles) {
        const jsonObj = JSON.stringify(window[obj.name]);
        fs.writeFile("./data/" + obj.src, jsonObj, (err) => {
            if (err) {
                console.error(err);
                return;
            };
        });
    }
};

// Modify the fields in our Armors so our CDB Armor objects
// look like the MV Armor objects.
DataManager.padCdbArmors = function(armorArray) {
    for (const obj of armorArray) {
        if (obj === null) continue;

        // Load Params from CDB
        for (param of obj.params) {
            obj[param.key] = param.value;
        }

        // Overwrite the default MV parameters
        obj.params = [
            obj.mhp || 0,
            obj.mmp || 0,
            obj.atk || 0,
            obj.def || 0,
            obj.mat || 0,
            obj.mdf || 0,
            obj.agi || 0,
            obj.luk || 0,
        ];

        // Load Icon from CDB. It's stored in a slightly
        // different format from MV's, so we have to convert it.
        const x = obj.icon_id.x / obj.icon_id.width;
        const y = obj.icon_id.y / obj.icon_id.height;
        obj.iconIndex = y * 16 + x;

        // Load description text from CDB by indexing into our text table.
        obj.description = $dataText[obj.description].text;

        // This example doesn't include traits, but you can easily add that
        // using the same method used to load icons and params.
        obj.traits = [];
    }
};

// =============================================================================
// SoundManager
//
// Quick example showing how to use our CDB to load custom sound effects that
// can be referenced by a string key, rather than by name.
//
// This allows you to expand the default sound database MV has. If you want a
// "Menu Open" sound effect, you can use this to call it by writing:
//
// SoundManager.playSound('menu_open');
//
// Without having to reference the actual filename. If you want to change the
// file used, you just have to modify the filename in the CDB. This lets you
// avoid having to change the actual game code.
// =============================================================================
Castle.CDB.Scene_Boot_start = Scene_Boot.prototype.start;
Scene_Boot.prototype.start = function() {
    Castle.CDB.preloadSoundEffects();
    Castle.CDB.Scene_Boot_start.call(this);
};

Castle.CDB.preloadSoundEffects = function() {
    const map = new Map();
    for (se of $dataSounds) {
        if (se === null) continue;
        map.set(se.key, se);
        AudioManager.loadStaticSe(se);
    }
    // Load sounds into a map indexed by CDB string key,
    // so they can be accessed more conveniently
    $dataSounds = map;
};

SoundManager.playSound = function(key) {
    AudioManager.playStaticSe($dataSounds.get(key));
};
